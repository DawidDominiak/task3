/*
* Program będący implementacją algorytmu zadania drugiego z Programowania.
* @author Dawid Dominiak
* @licence Unlicensed
*/
#include <iostream> //Wczytanie zależności
#include <climits>

using namespace std; //Używamy przestrzeni nazw std. Nie występują w kodzie żadne kolizje nazw.

/*
* Główna funkcja inicjalizująca program
*/
int main() {
	string input = ""; //Definicja z deklaracją zmiennych.
	char lastCharacterOfLastWord = '0'; //Zmienna posiadająca domyślną wartość bez biznesowego sensu.
	int shortestWordSize = INT_MAX;
	int lastLettersDuplicatedCounter = 0; //Zmienna z wartością ilości powtórzeń.
	do //rozpoczęcie pętli do ... while
	{
		cin >> input; //Przypisanie wyniku strumienia wejściowego do zmeinnej input.

		if(input == "end") { //Instrukcja warunkowa sprawdzająca czy zmienna input ma wartość "end".
			break; //Przerwanie pętli
		}

		if(lastCharacterOfLastWord != '0' && input[input.size()-1] == lastCharacterOfLastWord) { //sprawdzenie czy ostatni znak ostatniego słowa jest różne od domyślnej wartości i czy ostatni znak z aktualnej wartości zmiennej input jest równy wartości lastCharacterOfLastWord
			lastLettersDuplicatedCounter++; //Zwiększenie licznika lastLettersDuplicatedCounter o jeden
		}

		lastCharacterOfLastWord = input[input.size()-1]; //Przypisanie do zmiennej lastCharacterOfLastWord wartości ostatniego znaku ze zmiennej input typu string
		
		if(input.size() < shortestWordSize) { //Intrukcja warunkowa sprawdzająca czy ilość znaków w zmiennej input jest mniejsza od wartości zmiennej shortestWordSize
			shortestWordSize = input.size(); //Przypisanie do wartości shortestWordSize wartości input.size()
		}

	} while(true); //Warunek jest zawsze spełniony - działa, dopóki pętla nie zostanie przerwana.

	cout << "Dwa napisy konczace sie po sobie tym samym znakiem wczytano " << lastLettersDuplicatedCounter << " razy. Najkrótszy wyraz miał " << shortestWordSize << " znaków." <<endl; //Strumień wyjściowy przyjazny dla użytkownika.

	return 0;
}